// Arrray para vigilar el estado del juego y cada celda
var origBoard;
const huPlayer = "O";
const aiPlayer = "X";
// Combinaciones de celdas necesarias para ganar
const winCombos = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[6, 4, 2]
];

// Permite que el botón ejecute la función startGame
document.querySelector('button').
				addEventListener('click', startGame, false);

// Seleccionar todas las celdas
const cells = document.querySelectorAll(".cell");
// Iniciar el juego
startGame();

/**
 * Iniciar el juego
 */
function startGame() {
	document.querySelector(".message-block").style.height = '0';
	document.querySelector(".message").style.display = 'none';
	document.querySelector('.message-block').style.padding = '0';
	document.querySelector("button").style.display = 'none';
	// Crear un array con los números del 1 al 9
	origBoard = Array.from(Array(9).keys());
	// Limpar la celda de colores y marcas
	for (var i = 0; i < cells.length; i++) {
		cells[i].innerText = "";
		cells[i].style.removeProperty("background-color");
		cells[i].addEventListener("click", turnClick, false);
	}
}

/**
 * Revisa el id de la celda y ejecturá la funcion turn
 * para que los jugadores tomen su turno jugando
 * Revisa si existe un empate en el juego
 * @param {td} square  Celda cliqueada
 */
function turnClick(square) {
	// Si los jugadores no han jugado en la celda, ejecutar el turno
	if (typeof origBoard[square.target.id] == 'number') {
		turn(square.target.id, huPlayer);
		if (!checkWin(origBoard, huPlayer) && !checkTie())
			turn(bestSpot(), aiPlayer);
	}
}

/**
 * Agregar una O o una X al hacer click
 * @param {number} squareId
 * @param {string} player
 */
function turn(squareId, player) {
	origBoard[squareId] = player;
	document.getElementById(squareId).innerText = player;

	let gameWon = checkWin(origBoard, player);
	if (gameWon) gameOver(gameWon);
}

/**
 * Revisar cuál jugador ha ganado
 * @param {array} board
 * @param {string} player
 */
function checkWin(board, player) {
	/**
	 * Revisa las celdas en las que el jugador ya ha estado
	 * @param {array} a Array acumulador
	 * @param {any} e Elemento en la celda
	 * @param {number} i Index
	 */
	let plays = board.reduce((a, e, i) =>
			(e === player) ? a.concat(i) : a, []);
	let gameWon = null;
	for (let [index, win] of winCombos.entries()) {
		if (win.every( elem => plays.indexOf(elem) > -1)) {
			gameWon = { index: index, player: player };
			break;
		}
	}
	return gameWon;
}

/**
 * Finalizar el juego.
 * Usuario ya no puede hacer click
 * @param {any} gameWon
 */
function gameOver(gameWon) {
	for (let index of winCombos[gameWon.index]) {
		document.getElementById(index).style.backgroundColor =
			gameWon.player == huPlayer ? '#1976D2' : '#f44336';
	}
	for (var i = 0; i < cells.length; i++) {
		cells[i].removeEventListener('click', turnClick, false);
	}
	declareWinner(
		gameWon.player == huPlayer ? 'You win!' : 'You lose!'
	);
}

/**
 * Revisa los espacios en blanco
 * y juega en el mejor espacio posible
 * @returns el index donde la AI debería jugar
 */
function bestSpot() {
	return minimax(origBoard, aiPlayer).index;
}

function declareWinner(who) {
	document.querySelector('.message-block').style.height = '150px';
	document.querySelector('.message-block').style.padding = '15px';
	document.querySelector('.message-block .message').
					innerText = who;
	setTimeout(function() {
		document.querySelector("button").style.display = 'inline-block';
		document.querySelector(".message").style.display = 'block';
	}, 300);
}

/**
 * Revisa cuáles son los espacios en blanco
 *
 * @returns
 */
function emptySquares() {
	return origBoard.filter(s => typeof s == 'number');
}


/**
 * Si todas las celdas están llenas, return true, hay empate
 * Si no, return false, el juego sigue
 * @returns boolean
 */
function checkTie() {
	if (emptySquares().length == 0) {
		for (var i = 0; i < cells.length; i++) {
			cells[i].style.backgroundColor = '#4CAF50';
			cells[i].removeEventListener('click', turnClick, false);
		}

		declareWinner('Tie Game!');
		return true;
	}
	return false;
}


/**
 *
 * Elige la mejor celda para jugar
 * @param {array} newBoard
 * @param {string} player
 * @returns index de la celda a jugar
 */
function minimax(newBoard, player) {
	// Celdas vacías
	var availSpots = emptySquares(newBoard);
	// Establece un puntaje dependiendo de quién va ganando
	// y establece el 'estado' de la terminal
	// https://goo.gl/m7iyLx
	if (checkWin(newBoard, huPlayer)) {
		return { score: -10 };
	} else if (checkWin(newBoard, aiPlayer)) {
		return { score: 10 };
		// Ya no hay espacios para jugar y hay un empate
	} else if (availSpots.length === 0) {
		return { score: 0 };
	}
	// Recolectar los puntajes
	var moves = [];
	for (var i = 0; i < availSpots.length; i++) {
		var move = {};
		move.index = newBoard[availSpots[i]];
		// Coloca a la AI en el primero espacio vacío
		newBoard[availSpots[i]] = player;

		if (player == aiPlayer) {
			// Repite sus acciones, pero simulando al humano
			var result = minimax(newBoard, huPlayer);
			move.score = result.score;
		} else {
			// Repite sus acciones, pero simulando a la AI
			var result = minimax(newBoard, aiPlayer);
			move.score = result.score;
		}

		newBoard[availSpots[i]] = move.index;

		moves.push(move);
	}

	var bestMove;
	if (player === aiPlayer) {
		var bestScore = -10000;
		for (var i = 0; i < moves.length; i++) {
			if (moves[i].score > bestScore) {
				bestScore = moves[i].score;
				bestMove = i;
			}
		}
	} else {
		var bestScore = 10000;
		for (var i = 0; i < moves.length; i++) {
			if (moves[i].score < bestScore) {
				bestScore = moves[i].score;
				bestMove = i;
			}
		}
	}
	return moves[bestMove];
}

